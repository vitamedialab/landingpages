# landingpages

### Install Node modules
```
yarn install  // or npm install
```

### Starting server to edit the code
```
npm start
```

### Building to create HTML files
```
npm run build
```

For now we're pushing the `build` folder to the repo to have all the HTMLs here also.

---

#### Editing CSS

Edit CSS in `source/sass` folder.


#### Editing HTML

Edit PUG files in `source/pages` folder.


#### Adding a new landing page

1. Add a new item to array `PAGES` at the line 19 of the `webpack.config.js` file. (eg. new item = 'new-landing')

2. Create a folder with the same name in the `source/pages` folder. (eg. `source/pages/new-landing`)

3. Create the `pug`, `js`and `sass` files in the folder. (eg. `new-landing.js`, `new-landing.pug` and `new-landing.sass`). (Note: It's easier to copy a folder of another landing page and delete the files you don't need.)
