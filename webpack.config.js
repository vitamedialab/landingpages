const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const merge = require('webpack-merge');

const pug = require('./webpack/pug');
const devserver = require('./webpack/devserver');
const sass = require('./webpack/sass');
const extractCss = require('./webpack/css.extract');
const uglifyJS = require('./webpack/js.uglify');
const images = require('./webpack/images');
const cleanFolders = require('./webpack/clean.folders');

const PATHS = {
	source: path.join(__dirname, 'source'),
	build: path.join(__dirname, 'build')
};

const PAGES = [
	'landing1',
	'landing2',
	'landing4'
];

const getEntries = entriesArr => entriesArr.reduce((obj, pageStr) => {
		obj[pageStr] = PATHS.source + '/pages/'+ pageStr +'/'+ pageStr +'.js'
		return obj;
	}, {});

const getHTMLPlugins = pagesArr => {
	return pagesArr.map((pageName, i) => new HtmlWebpackPlugin({
			filename: `${(i === 0) ? 'index' : pageName}.html`,
			chunks: ['vendor', pageName],
			hash: true,
			template: `${PATHS.source}/pages/${pageName}/${pageName}.pug`
		})
	);
};

const entries = getEntries(PAGES)
entries.vendor = [
	'jquery', 'jsrender'
];

const common = merge([
	{
		entry: entries,
		output: {
			path: PATHS.build,
			filename: 'js/[name].js'
		},
		plugins: [
			new webpack.optimize.CommonsChunkPlugin({
				name: 'vendor'
			}),
		].concat(getHTMLPlugins(PAGES)),
	},
	pug(),
    images()
]);



module.exports = function(env) {
	if(env === 'production') {
		return merge([
			common,
			cleanFolders(),
            extractCss(),
			uglifyJS()
		])
	}

	if(env === 'development') {
		return merge([
			common,
			devserver(),
			extractCss()
		])
	}
}
