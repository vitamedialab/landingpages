const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');

module.exports = function (paths) {
    return {
        module: {
            rules: [
                {
                    test: /\.css$/,
                    include: paths,
                    // use: ['css-hot-loader'].concat(ExtractTextWebpackPlugin.extract({
                    //     fallback: 'style-loader',
                    //     use: 'css-loader'
                    // }))
                    use: ExtractTextWebpackPlugin.extract({
                        publicPath: '../',
                        fallback: 'style-loader',
                        use: ['css-loader']
                    })
                },
                {
                    test: /\.(sass|scss)$/,
                    include: paths,
                    use: ExtractTextWebpackPlugin.extract({
                        publicPath: '../',
                        filename: 'css/[name]-[hash].css',
                        fallback: 'style-loader',
                        use: [
                            {
                                loader: 'css-loader'
                            },
                            {
                                loader: 'sass-loader',
                                options: {
                                    includePaths: ['source/sass']
                                    // includePaths: ['source/sass', 'normalize.css']
                                }
                            }
                        ]
                    })
                }
            ]
        },
        plugins: [
            new ExtractTextWebpackPlugin({
                filename: './css/[name].css',
                // allChunks: true,
                // disable: true
            })
        ]
    }
};
