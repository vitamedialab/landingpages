module.exports = function() {
    return {
        devServer: {
            host: '0.0.0.0',
            disableHostCheck: true,
            stats: 'errors-only',
            port: 9000
        }
    }
}
