const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = function (folders = ['build', 'dist']) {
    return {
        plugins: [
            new CleanWebpackPlugin(folders)
        ]
    }
}
