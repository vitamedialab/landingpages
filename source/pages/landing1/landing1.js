import './landing1.sass';
import '../../scripts/aws.js';

// Getting libs
var $ = require('jquery');
var jsrender = require('jsrender')($);

// You can use 'jsrender' var or $.templates
jsrender.views.settings.allowCode(true);

// Render questions fields
var questionsTpl = jsrender.templates('#questions');
$('#questions-container').html( questionsTpl(surveyConfig.questions) );

// Render user fields
var userTpl = jsrender.templates('#user-fields');
$('#user-fields-container').html( userTpl(surveyConfig.userData) )
    // removing #user-fields-container tag
    .replaceWith(function(){ return $(this).html() });

// Render Logos
var logosTpl = jsrender.templates('#logos');
$('#logos-container').html( logosTpl({logos: logos}) )


// ---------------

// Form: validation and submit
var form = $('form'),
    firstStep  = $('#first-step'),
    secondStep = $('#second-step'),
    modal = $('#modal');

$('#btn-next')
    .on('click', function(e){
        firstStep.hide();
        secondStep.show();
    });

function toggleModal(){
    document.body.classList.toggle('is-modal-open');
}

// Open modal
$('#open-modal, #close-modal').on('click', function(e){
    e.preventDefault();
    toggleModal();
});

// Send form
function sendForm(form) {
    var formData = form.serialize();

    console.log('[Form submit] Sending!', formData);

    $.ajax({
        method: "POST",
        url: "/api/coreg",
        data: formData,
        success: function (url) {
            console.log('[Form submit] Sucess!', url);
            // swal(
            //     'Mange tak!',
            //     'Dine informationer er nu gemt!',
            //     'success'
            // );
            // setTimeout(function () {
            //     location.href = url;
            // }, 2000);
        }
    });
}

form.on('submit', function(e){
    e.preventDefault();
    sendForm(form);
})
