var leadCampaigns = [
    { label: "Mail excellent (Jackiejackpot.dk) Sendes daglig", id: 23 },
    { label: "Mail excellent (Vitafinans) Sendes daglig", id: 1 },
    { label: "Mail excellent (Clickfinans) Sendes daglig", id: 46 },
];
var logos = [
    { url: "https://pbs.twimg.com/profile_images/748825919192793088/qtFsAZKi_400x400.jpg", alt:  "tdc"},
    { url: "https://www.mobilespoint.com/wp-content/uploads/2016/03/Telenor-Postpaid-Packages.png", alt:  "Telenor"},
];
var surveyConfig = {
    questions:[
        {

            text: "Boligforhold",
            layoutColumns: 3,
            fields: [
                {
                    type: "radio",
                    name: "boligforhold",
                    options: [
                        {"label": "Ejerbolig", "value" : "23"},
                        {"label": "Lejebolig", "value" : "11"}
                    ]
                }
            ]
        },
        {

            text: "Jobsituation",
            fields: [
                {
                    type: "select",
                    name: "jobsituation",
                    placeholder: "Select an option",
                    options: [
                        {"label": "lønmodtager", "value" : "lønmodtager"},
                        {"label": "selvstændig", "value" : "selvstændig"}
                    ]
                }
            ]
        },
        {

            text: "Hvilke forsikringer har du i dag?",
            layoutColumns: 5,
            fields: [
                {
                    type : "checkbox",
                    name: "forsikringer",
                    options : [
                        {label: "Indbo", value: "Indbo"},
                        {label: "Bil", value: "Bil"},
                        {label: "Sundhed", value: "Sundhed"},
                        {label: "Ulikke", value: "Ulikke"},
                        {label: "Hus", value: "Hus"},
                        {label: "Dyr", value: "Dyr"},
                        {label: "Liv", value: "Liv"}
                    ]
                }
            ]
        },
        {

            text: "Hvad er dit teleselskab?",
            fields: [
                {
                    type : "select",
                    name: "telecom-company",
                    options : [
                        {label: "Telia", value: "Telia"},
                        {label: "Call me ", value: "Call me "},
                        {label: "DLG ", value: "DLG "},
                        {label: "Telenor ", value: "Telenor "},
                        {label: "Telmore", value: "Telmore"},
                        {label: "Cbb", value: "Cbb"},
                        {label: "Tdc", value: "Tdc"},
                        {label: "3Mobil", value: "3Mobil"},
                        {label: "Fullrate", value: "Fullrate"},
                        {label: "Plenti", value: "Plenti"},
                        {label: "Oister", value: "Oister"},
                        {label: "Andet", value: "Andet"},
                    ]
                },
                {
                    type : "checkbox",
                    name: "telecom-company-type",
                    options : [
                        {label: "Privat", value: "Privat"},
                        {label: "Firma", value: "Firma"},
                    ]
                }
            ]
        },
    ],
    userData: [
        {
            name: "Fornavn", // required
            label: "Fornavn", // optional, otrw NAME will be used
            // placeholder: "Fornavnnn", // optional
            required: true, // optional
            type : "text"
        },
        {
            name: "efternavn",
            label: "Efternavn",
            // placeholder: "Efternavn",
            type : "text",
            required: true,
        },
        {
            name: "email",
            label: "E-mail",
            type : "email",
            required: true,
        },
        {
            name: "mobilnummer",
            label: "Mobilnummer",
            type : "number",
            required: true,
        },
        {
            name: "gender",
            label: "Køn",
            type : "select",
            required: true,
            options: [
                {label: 'Mand', value: 'Mand'},
                {label: 'Kvinde', value: 'Kvinde'},
            ]
        },
        {
            name: "birthday",
            label: "Fødselsdag",
            type : "date",
            required: true,
        },
    ]
};
