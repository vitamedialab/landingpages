import './landing2.sass';
import '../../scripts/aws.js';

var $ = require('jquery');

var firstStep  = $('#first-step'),
    secondStep = $('#second-step'),
    backBtn = $('#back-button'),
    modal = $('#modal'),
    hiddenClass = 'l-hidden';

// -- Back button
backBtn.on('click', function(e){
    e.preventDefault();
    showFirstStep();
});

function showFirstStep(){
    firstStep.removeClass(hiddenClass);
    secondStep.addClass(hiddenClass);
}


// -- Modal
function toggleModal(modal){
    document.body.classList.toggle('is-modal-open');
    modal.toggleClass('is-open');
}

$('#open-modal, #close-modal').on('click', function(e){
    e.preventDefault();
    toggleModal(modal);
});

// -- First step
firstStep.on('submit', function(e){
    e.preventDefault();
    console.log('First step!');
    showSecondStep();
});

function showSecondStep(){
    firstStep.addClass(hiddenClass);
    secondStep.removeClass(hiddenClass);
}
// showSecondStep();


// -- Second step
secondStep.on('submit', function(e){
    e.preventDefault();
    console.log('Second step!', firstStep.serialize() + '&' + secondStep.serialize());
    // sendForm(form);
});

// Send form
function sendForm(form) {
    var formData = form.serialize();

    console.log('[Form submit] Sending!', formData);

    $.ajax({
        method: "POST",
        url: "/api/coreg",
        data: formData,
        success: function (url) {
            console.log('[Form submit] Sucess!', url);
        }
    });
}
