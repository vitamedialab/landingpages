var $ = require('jquery');

var host = "http://dawa.aws.dk";
var corssupported = "withCredentials" in (new XMLHttpRequest());
var autocomplete = $('#autocomplete');

// var info = $("#adresseinfo");
// var ul = autocomplete;
var input = $('#my_address');
var listData;

function visAdresseInfo(url) {
    $.ajax({
        url: url,
        dataType: corssupported ? "json" : "jsonp"
    })
        .fail(function (jqXHR, textStatus) {
            alert(jqXHR.status + " " + jqXHR.statusText); // kaldes ikke ved jsonp
        })
        .then(function (adresse, textStatus, jqXHR) {
            $('#my_address').val(adresse.adressebetegnelse);
            // $('#my_address').val(adresse.adressebetegnelse + ' <i class="fa fa-check text-success"></i>');

            $('[name="street"]').val(adresse.adgangsadresse.vejstykke.navn);
            $('[name="street_no"]').val(adresse.adgangsadresse.husnr);
            $('[name="floor"]').val((adresse.etage ? adresse.etage + '.' : '') + ' ' + (adresse.dør ? adresse.dør + '.' : ''));
            $('[name="zip"]').val(adresse.adgangsadresse.postnummer.nr);
            $('[name="city"]').val(adresse.adgangsadresse.postnummer.navn);

            $('#second_container').fadeIn('fast');
            // $('.btn-finish').show();
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);
        });
}

function hentliste(q, caretpos) {
    // info.html("");
    // info.listview("refresh");
    // ul.listview("refresh");

    var parametre = {};
    parametre.side = 1;
    parametre.per_side = 10;
    parametre.q = q;
    parametre.type = "adresse";
    parametre.fuzzy = true;
    parametre.caretpos = caretpos;
    $.ajax({
        url: host + "/autocomplete",
        dataType: corssupported ? "json" : "jsonp",
        data: parametre
    })
        .then(function (response) {
            if (response.length === 1) {
                valgt(response[0]);
            }
            else {
                var itemsHTML = '';
                listData = response;

                $.each(response, function (i, val) {
                    itemsHTML += "<li id='" + i + "' data-val="+ val +">" + val.forslagstekst + "</li>";

                    // $("#" + i).bind("vclick", valgItem(val));
                });

                fillAutocompleteList(itemsHTML);
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);
        });
}

function valgt(valg) {
    input.val(valg.tekst);
    if (valg.type === "adresse") {
        visAdresseInfo(valg.data.href);
    }
    else {
        hentliste(valg.tekst, valg.caretpos);
    }
}

function valgItem(valg) {
    // return function (e) {
    valgt(valg);

    cleanAutocompleteList();
    input.val(valg.tekst);
    input.focus();

    // return false;
    // };
}

var autoTimeout;
function autocompleteCallback(e, data) {
    if(autoTimeout) clearTimeout(autoTimeout);

    autoTimeout = setTimeout(function(){
        var value = input.val();

        cleanAutocompleteList();
        if (value && value.length > 1) {
            hentliste(value, value.length);
        }
    }, 400);
};

input.on("keyup", autocompleteCallback);
// autocomplete.on("filterablebeforefilter", autocompleteCallback);

function cleanAutocompleteList(){
    autocomplete.html('').addClass('empty');
}

function fillAutocompleteList(htmlStr){
    autocomplete.html(htmlStr).removeClass('empty');
}

function getLocationByText(text){
    return listData.filter(function(d){
        return d.forslagstekst.trim() === text
    }).pop();
}

// Autocomplete list item click
function onAutocompleteClick(e){
    var itemData = getLocationByText(e.target.innerText);
    valgItem(itemData);
}
autocomplete.on('click', 'li', onAutocompleteClick);
